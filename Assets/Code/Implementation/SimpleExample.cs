﻿using FuzzyLogicPCL;
using FuzzyLogicPCL.FuzzySets;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace FuzzyLogicApp
{
    class SimpleExample : MonoBehaviour
    {
        public static TextMeshProUGUI text;
        void Start()
        {
            //Texto que sustituye a la consola
           
            text = GameObject.Find("ConsoleText").GetComponent<TextMeshProUGUI>();

            // Creación del sistema
            WriteLine("Gestión del zoom GPS", true);
            FuzzySystem system = new FuzzySystem("Gestión del zoom GPS");

            WriteLine("1) Agregar las variables", true);

            // Agregar la variable lingüística "Distancia" (de 0 a 500 000 m)
            WriteLine("Agregar la variable Distancia");
            LinguisticVariable distancia = new LinguisticVariable("Distancia", 0, 500000);
            distancia.AddValue(new LinguisticValue("Poca", new LeftFuzzySet(0, 500000, 30, 50)));
            distancia.AddValue(new LinguisticValue("Media", new TrapezoidalFuzzySet(0, 500000, 40, 50, 100, 150)));
            distancia.AddValue(new LinguisticValue("Mucha", new RightFuzzySet(0, 500000, 100, 150)));
            system.addInputVariable(distancia);

            // Agregar la variable lingüística "Velocidad" (de 0 à 200)
            WriteLine("Agregar la variable Velocidad");
            LinguisticVariable velocidad = new LinguisticVariable("Velocidad", 0, 200);
            velocidad.AddValue(new LinguisticValue("Lenta", new LeftFuzzySet(0, 200, 20, 30)));
            velocidad.AddValue(new LinguisticValue("PocoRapida", new TrapezoidalFuzzySet(0, 200, 20, 30, 70, 80)));
            velocidad.AddValue(new LinguisticValue("Rapida", new TrapezoidalFuzzySet(0, 200, 70, 80, 90, 110)));
            velocidad.AddValue(new LinguisticValue("MuyRapida", new RightFuzzySet(0, 200, 90, 110)));
            system.addInputVariable(velocidad);

            // Agregar la variable lingüística "Zoom" (de 1 à 5)
            WriteLine("Agregar la variable Zoom");
            LinguisticVariable zoom = new LinguisticVariable("Zoom", 0, 5);
            zoom.AddValue(new LinguisticValue("Bajo", new LeftFuzzySet(0, 5, 1, 2)));
            zoom.AddValue(new LinguisticValue("Normal", new TrapezoidalFuzzySet(0, 5, 1, 2, 3, 4)));
            zoom.AddValue(new LinguisticValue("Grande", new RightFuzzySet(0, 5, 3, 4)));
            system.addOutputVariable(zoom);

            WriteLine("2) Agregar las reglas", true);

            // Creación de las reglas en función de la matriz siguiente:
            // Cuanto menor sea el zoom, más lejos se verá (pero menos detallado)
            // V \ D    || P | M | Mucha |
            // Lenta    || N | B | B     |
            // Poco Ráp || N | N | B     |
            // Rápida   || G | N | B     |
            // Muy Ráp  || G | G | B     |
            system.addFuzzyRule("IF Distancia IS Mucha THEN Zoom IS Bajo");
            system.addFuzzyRule("IF Distancia IS Poca AND Velocidad IS Lenta THEN Zoom IS Normal");
            system.addFuzzyRule("IF Distancia IS Poca AND Velocidad IS PocoRapida THEN Zoom IS Normal");
            system.addFuzzyRule("IF Distancia IS Poca AND Velocidad IS Rapida THEN Zoom IS Grande");
            system.addFuzzyRule("IF Distancia IS Poca AND Velocidad IS MuyRapida THEN Zoom IS Grande");
            system.addFuzzyRule("IF Distancia IS Media AND Velocidad IS Lenta THEN Zoom IS Bajo");
            system.addFuzzyRule("IF Distancia IS Media AND Velocidad IS PocoRapida THEN Zoom IS Normal");
            system.addFuzzyRule("IF Distancia IS Media AND Velocidad IS Rapida THEN Zoom IS Normal");
            system.addFuzzyRule("IF Distancia IS Media AND Velocidad IS MuyRapida THEN Zoom IS Grande");
            WriteLine("9 reglas agregadas \n");

            WriteLine("3) Resolución de casos prácticos", true);
            // Caso práctico 1: Velocidad de 35 kms/h, próximo cambio de dirección a 70m
            WriteLine("Caso 1:", true);
            WriteLine("V = 35 (poco rápida)");
            WriteLine("D = 70 (media)");
            system.SetInputVariable(velocidad, 35);
            system.SetInputVariable(distancia, 70);
            WriteLine("Esperado: zoom normal, centroide en 2.5");
            WriteLine("Resultado: " + system.Solve() + "\n");

            // Caso práctico 2: Velocidad de 25 kms/h, próximo cambio de dirección a 70m
            system.ResetCase();
            WriteLine("Caso 2:", true);
            WriteLine("V = 25 (50% lenta, 50% poco rápida)");
            WriteLine("D = 70 (media)");
            system.SetInputVariable(velocidad, 25);
            system.SetInputVariable(distancia, 70);
            WriteLine("Esperado: zoom normal al 50% + zoom bajo al 50%");
            WriteLine("Resultado: " + system.Solve() + "\n");

            // Caso práctico 3: Velocidad de 72.5 kms/h, próximo cambio de dirección a 40m
            system.ResetCase();
            WriteLine("Caso 3:", true);
            WriteLine("V = 72.5 (75% poco rápida + 25% rápida)");
            WriteLine("D = 40 (50% poca)");
            system.SetInputVariable(velocidad, 72.5);
            system.SetInputVariable(distancia, 40);
            WriteLine("Esperado: zoom normal al 50% + zoom grande al 25%");
            WriteLine("Resultado: " + system.Solve() + "\n");

            // Caso práctico 4: Velocidad de 100 kms/h, próximo cambio de dirección a 110m
            system.ResetCase();
            WriteLine("Caso 4:", true);
            WriteLine("V = 100 (50% rápida + 50% muy rápida)");
            WriteLine("D = 110 (80% media, 20% grande)");
            system.SetInputVariable(velocidad, 100);
            system.SetInputVariable(distancia, 110);
            WriteLine("Esperado: zoom bajo al 20% + zoom normal al 50% + zoom grande al 50%");
            WriteLine("Resultado: " + system.Solve() + "\n");

            // Caso práctico 5 : Velocidad de 45 kms/h, próximo cambio de dirección a 160m
            system.ResetCase();
            WriteLine("Caso 5:", true);
            WriteLine("V = 45 (100% poco rápida)");
            WriteLine("D = 160 (100% mucha)");
            system.SetInputVariable(velocidad, 45);
            system.SetInputVariable(distancia, 160);
            WriteLine("Esperado: zoom bajo al 100%");
            WriteLine("Resultado: " + system.Solve() + "\n");
            /*
            while (true) ;
            */
        }

        /// <summary>
        /// Ayuda para escribir mensajes por consola (agregando *)
        /// </summary>
        /// <param name="msg">Mensaje a mostrar</param>
        /// <param name="stars">¿Necesita asteriscos?</param>
        private static void WriteLine(string msg, bool stars = false)
        {
            if (stars)
            {
                msg = "*** " + msg + " ";
                while (msg.Length < 45)
                {
                    msg += "*";
                }
            }
            //Console.WriteLine(msg);

            text.text += msg;
            text.text += "\n";
        }
    }
}
