﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

namespace FuzzyLogicApp
{
    public class EnemyController : MonoBehaviour
    {

        public Transform spawnFireballPosition;
        public GameObject FireBall;
        public FuzzyManager myFuzzyManager;
        public TextMeshProUGUI Distancetext;
        public TextMeshProUGUI Mptext;
        public TextMeshProUGUI DamageText;
        public float distance;
        public float mp;
        public float damage;
        private Vector3 maxdistance;
        private Vector3 mindistance;
        public GameObject anim;
        // Start is called before the first frame update
        void Start()
        {
            mindistance = new Vector3(-33.69f, 0.5f, -5.61f);
            maxdistance = new Vector3(28.9f, 0.5f, -5.61f);
            distance = 700;
            mp = 1;
            damage = 0;
        }

        // Update is called once per frame
        void Update()
        {
            transform.position = Vector3.Lerp(mindistance, maxdistance, distance / 1000);
            Distancetext.text = "Magic Power: " + mp;
            Mptext.text = "Distance: " + distance;
        }

        public void DoFuzzyLogic()
        {
            SpawnFireball();
            DamageText.text = "Damage " + ((float)myFuzzyManager.ResolveFuzzyCase((int)mp, (int)distance)).ToString("#.00");

        }

        public void SpawnFireball()
        {
            anim.GetComponent<Animator>().SetBool("Attack", true);
            Instantiate(FireBall, spawnFireballPosition.position, transform.rotation);
        }

        public void AdjustMp(float _mp)
        {
            mp = _mp;
        }
        public void AdjustDistance(float _distance)
        {
            distance = _distance;
        }
    }
}