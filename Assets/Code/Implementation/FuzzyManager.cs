﻿using FuzzyLogicPCL;
using FuzzyLogicPCL.FuzzySets;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace FuzzyLogicApp
{
    public class FuzzyManager : MonoBehaviour
    {
        FuzzySystem system;
        LinguisticVariable distance;
        LinguisticVariable mp;
        LinguisticVariable damage;
        public static TextMeshProUGUI text;

        void Start()
        {
            //Texto que sustituye a la consola
            text = GameObject.Find("ConsoleText").GetComponent<TextMeshProUGUI>();

            // Creación del sistema
            WriteLine("Calculo de daño con FuzzyLogic", true);
            system = new FuzzySystem("Gestion del daño");

            WriteLine("1) Agregar las variables", true);

            // Agregar la variable lingüística "Distancia" (de 0 a 10000 m)
            WriteLine("Agregar la variable Distancia");
            distance = new LinguisticVariable("Distance", 0, 1000);
            distance.AddValue(new LinguisticValue("Melee", new LeftFuzzySet(0, 1000, 0, 50)));
            distance.AddValue(new LinguisticValue("MediumRange", new TrapezoidalFuzzySet(0, 1000, 40, 60, 250, 300)));
            distance.AddValue(new LinguisticValue("LongRange", new TrapezoidalFuzzySet(0, 1000, 280, 400, 650, 800)));
            distance.AddValue(new LinguisticValue("SuperLongRange", new RightFuzzySet(0, 1000, 750, 1000)));
            system.addInputVariable(distance);

            // Agregar la variable lingüística "Poder Magico" (de 0 a 500)
            WriteLine("Agregar la variable Magical Power");
            mp = new LinguisticVariable("MP", 0, 500);
            mp.AddValue(new LinguisticValue("Novice", new LeftFuzzySet(0, 500, 0, 100)));
            mp.AddValue(new LinguisticValue("Apprentice", new TrapezoidalFuzzySet(0, 500, 80, 110, 150, 170)));
            mp.AddValue(new LinguisticValue("Adept", new TrapezoidalFuzzySet(0, 500, 165, 190, 300, 340)));
            mp.AddValue(new LinguisticValue("Expert", new TrapezoidalFuzzySet(0, 500, 330, 350, 390, 460)));
            mp.AddValue(new LinguisticValue("Master", new RightFuzzySet(0, 500, 490, 500)));
            system.addInputVariable(mp);

            // Agregar la variable lingüística "Daño" (de 0 a 100)
            WriteLine("Agregar la variable Damage");
            damage = new LinguisticVariable("Damage", 0, 100);
            damage.AddValue(new LinguisticValue("Low", new LeftFuzzySet(0, 100, 0, 20)));
            damage.AddValue(new LinguisticValue("Medium", new TrapezoidalFuzzySet(0, 100, 15, 25, 40, 50)));
            damage.AddValue(new LinguisticValue("High", new TrapezoidalFuzzySet(0, 100, 45, 55, 75, 85)));
            damage.AddValue(new LinguisticValue("Mortal", new RightFuzzySet(0, 100, 80, 100)));
            system.addOutputVariable(damage);

            WriteLine("2) Agregar las reglas", true);

            //system.addFuzzyRule("IF Distance IS SuperLongRange THEN Damage IS Low");

            system.addFuzzyRule("IF Distance IS Melee AND MP IS Novice THEN Damage IS Medium");
            system.addFuzzyRule("IF Distance IS Melee AND MP IS Apprentice THEN Damage IS High");
            system.addFuzzyRule("IF Distance IS Melee AND MP IS Adept THEN Damage IS High");
            system.addFuzzyRule("IF Distance IS Melee AND MP IS Expert THEN Damage IS Mortal");
            system.addFuzzyRule("IF Distance IS Melee AND MP IS Master THEN Damage IS Mortal");

            system.addFuzzyRule("IF Distance IS MediumRange AND MP IS Novice THEN Damage IS Low");
            system.addFuzzyRule("IF Distance IS MediumRange AND MP IS Apprentice THEN Damage IS Medium");
            system.addFuzzyRule("IF Distance IS MediumRange AND MP IS Adept THEN Damage IS High");
            system.addFuzzyRule("IF Distance IS MediumRange AND MP IS Expert THEN Damage IS Mortal");
            system.addFuzzyRule("IF Distance IS MediumRange AND MP IS Master THEN Damage IS Mortal");

            system.addFuzzyRule("IF Distance IS LongRange AND MP IS Novice THEN Damage IS Low");
            system.addFuzzyRule("IF Distance IS LongRange AND MP IS Apprentice THEN Damage IS Low");
            system.addFuzzyRule("IF Distance IS LongRange AND MP IS Adept THEN Damage IS Medium");
            system.addFuzzyRule("IF Distance IS LongRange AND MP IS Expert THEN Damage IS High");
            system.addFuzzyRule("IF Distance IS LongRange AND MP IS Master THEN Damage IS Mortal");

            system.addFuzzyRule("IF Distance IS SuperLongRange AND MP IS Novice THEN Damage IS Low");
            system.addFuzzyRule("IF Distance IS SuperLongRange AND MP IS Apprentice THEN Damage IS Low");
            system.addFuzzyRule("IF Distance IS SuperLongRange AND MP IS Adept THEN Damage IS Low");
            system.addFuzzyRule("IF Distance IS SuperLongRange AND MP IS Expert THEN Damage IS Medium");
            system.addFuzzyRule("IF Distance IS SuperLongRange AND MP IS Master THEN Damage IS High");
            WriteLine("21 reglas agregadas \n");
            /*
            WriteLine("3) Resolución de casos prácticos", true);
            // Caso práctico 1: Velocidad de 35 kms/h, próximo cambio de dirección a 70m
            WriteLine("Caso 1:", true);
            WriteLine("MP = 190 (Adept)");
            WriteLine("Distance = 500 (LongRange)");
            system.SetInputVariable(mp, 190);
            system.SetInputVariable(distance, 500);
            WriteLine("Resultado: " + system.Solve() + "\n");
            */
        }

        public double ResolveFuzzyCase(int fuzzyvalue1, int fuzzyvalue2)
        {
            system.ResetCase();
            system.SetInputVariable(mp, fuzzyvalue1);
            system.SetInputVariable(distance, fuzzyvalue2);
            return system.Solve();
        }

        private static void WriteLine(string msg, bool stars = false)
        {
            if (stars)
            {
                msg = "*** " + msg + " ";
                while (msg.Length < 45)
                {
                    msg += "*";
                }
            }
            text.text += msg;
            text.text += "\n";
        }
    }
}
